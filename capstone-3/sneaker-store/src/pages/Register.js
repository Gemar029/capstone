import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext.js'


export default function Register(){
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [userName, setUserName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState ('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)

	// we consume the setUser function from the UserContext
	const {setUser} = useContext(UserContext); 

	// we contain useNavigate to navigate variagble
	const navigate = useNavigate();
	// We have to use the useEffect in enabling the submit button
	useEffect(() => {
		if(firstName !== '' && lastName !== '' && userName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '' && password1 === password2 && mobileNo.length > 10 && password1.length > 3){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}
	}, [firstName, lastName, userName, email, mobileNo, password1, password2])

	// function that wil be triggered once we submit
	function register(event){

		event.preventDefault()

		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'))

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				userName: userName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})

		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successfully registered!',
					icon: 'success',
					text: 'Thank you for registering'
				})
				navigate('/login')
			}else{
				Swal2.fire({
				    title: 'Email is already registered',
				    icon: 'error',
				    text: 'Try logging in or use different email to register.'
				})
			}
		})
		
	}
return(
	<Container className = "mt-5">
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>

				<Form onSubmit = {event => register(event)}>
				    <Form.Group className="mb-3" controlId="formBasicFirstName">
				        <Form.Label>FirstName</Form.Label>
				        <Form.Control 
				        	type="text"
				        	value = {firstName} 
				        	onChange = {event => {
				        	
				        		setFirstName(event.target.value)
				        		// console.log(firstName)
				        	}}
				        	placeholder="Enter your First Name" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicLastName">
				        <Form.Label>LastName</Form.Label>
				        <Form.Control 
				        	type="text"
				        	value = {lastName} 
				        	onChange = {event => {
				        	
				        		setLastName(event.target.value)
				        		// console.log(lastName)
				        	}}
				        	placeholder="Enter your Last Name" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicUserName">
				        <Form.Label>UserName</Form.Label>
				        <Form.Control 
				        	type="text"
				        	value = {userName} 
				        	onChange = {event => {
				        	
				        		setUserName(event.target.value)
				        		// console.log(userName)
				        	}}
				        	placeholder="Enter your UserName" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email"
				        	value = {email} 
				        	onChange = {event => {
				        	
				        		setEmail(event.target.value)
				        		// console.log(email)
				        	}}
				        	placeholder="Enter email" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicMobileNo">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				        	type="tel"
				        	value = {mobileNo} 
				        	onChange = {event => {
				        	
				        		setMobileNo(event.target.value)
				        		// console.log(mobileNo)
				        	}}
				        	placeholder="Mobile Number" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1} 
				        	onChange = {event => {
				        	
				        		setPassword1(event.target.value)
				        		// console.log(password1)
				        	}}
				        	placeholder="Password" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => {
				        	
				        		setPassword2(event.target.value)
				        		// console.log(password2)
				        	}} 
				        	placeholder="Confirm Password" />
				    </Form.Group>

				    <p>Have an account already ? <Link to = "/login">Log in here</Link></p>
				    <Button variant="primary" type="submit" disabled = {isDisabled}>Submit</Button>
				</Form>
			</Col>
		</Row>
	</Container>
	)
}
