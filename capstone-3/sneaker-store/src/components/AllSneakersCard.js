import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link, useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function SneakersCard({onArchive, onActivate, ...props}){
	const {user} = useContext(UserContext)

	const {_id, name, description, price, size, stocks, isActive} = props.sneakerProp;
	// const[active, setIsActive] = useState(true)

	// const[sneakerIsActive, setSneakerIsActive] = useState(false);

	// 	useEffect (() => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/sneakers/${_id}/archive`,{
	// 		method: 'PATCH',
	// 		headers: {
	// 			'Authorization': `Bearer ${localStorage.getItem('token')}`,
	// 			'Content-Type': 'application/json'
	// 		},
	// 		body: JSON.stringify({
	// 			isActive: isActive
	// 		})


	// 	})
	// 	.then(result => result.json())
	// 	.then(data => {
	// 		isActive: setIsActive
	// 	})
	// },[])

	// const archive = (e) => {
	// 	e.preventDefault();
	// 	fetch(`${process.env.REACT_APP_API_URL}/sneakers/${_id}/archive`, {
	// 		method: 'PATCH',
	// 		headers: {
	// 			'Authorization': `Bearer ${localStorage.getItem('token')}`,
	// 			'Content-Type': 'application/json'
	// 		},
	// 		body: JSON.stringify({
	// 			isActive: active
	// 		})

	// 	})
	// 	.then(response => response.json())
	// 	.then(data =>{
	// 		if(data){
	// 			if(active){

	// 				Swal2.fire({
	// 				    title: 'Successfully Archived!',
	// 				    icon: 'success',
	// 				    text: 'You have successfully unArchived this course!'
	// 				});
	// 			}else{
	// 				Swal2.fire({
	// 				    title: 'Successfully Archived!',
	// 				    icon: 'success',
	// 				    text: 'You have successfully UnArchived this course!'
	// 				});
	// 			}
	// 		}else{
	// 			Swal2.fire({
    //                     title: 'Something went wrong',
    //                     icon: 'error',
    //                     text: 'Please try again or make sure you are login as Admin'
    //                 })
	// 		}
	// 	})
	// }

	
	return(
		<Col className = "col-12 col-md-4 mx-auto mt-3">
					<Card className = "d-flex align-items-start">
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <br/>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>
					        {description}
					        </Card.Text>

					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>
					        PHP {price}
					        </Card.Text>

					        <Card.Subtitle>Size</Card.Subtitle>
					        <Card.Text>
					        US {size}
					        </Card.Text>

					        <Card.Subtitle>Stocks</Card.Subtitle>
					        <Card.Text>
					        {stocks}
					        </Card.Text>

					        <Card.Subtitle>Available</Card.Subtitle>
					        <Card.Text>
					        {isActive ===true ? 'Yes' : 'No'}
					        </Card.Text>
					        <Button as = {Link} to ={`/allSneakers/${_id}`} className = 'me-2'>Update</Button>
{/*					        {
					        	isActive === true
					        	?
					        	<Button  onClick = {e => {
					        		setIsActive(false)
					        		archive(e)
					        	}}>DeActivate</Button>
					        	:
					        	<Button  onClick = {e => {
					        		setIsActive(true)
					        		archive(e)
					        	}}>Activate</Button>	
					        }	*/}
					        					        {
					        	isActive === true
					        	?
					        	<Button  onClick = {onArchive}>DeActivate</Button>
					        	:
					        	<Button  onClick = {onActivate}>Activate</Button>	
					        }
{/*					        <Button className = 'me-2'  onClick = {onArchive}>DeActivate</Button>
					        <Button className = 'me-2'  onClick = {onActivate}>Activate</Button>*/}


					      </Card.Body>
					    </Card>
				</Col>
	)
}