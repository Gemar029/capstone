const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: [true, "User Id for order is required"]
	},

	name: {
		type: String,
		required: [true, "Name in the Order is required"]
	},

	sneakers: [{

			sneakerId: {
				type: mongoose.Schema.Types.ObjectId,
				required: [true, "Sneaker Id of order is required"]
			},
			sneakerName:{
				type: String,
				required: [true, "Sneaker Name of order is required"]

			},

			quantity: {
				type: Number,
				required: [true, "Quantity of a product is required"]
			}
		
		}],

	totalAmount: {
		type: Number,
		required: [true, "Total amount of order is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}


})

const Orders = mongoose.model("Orders", orderSchema);
module.exports = Orders;