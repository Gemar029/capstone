import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddSneaker from './pages/AddSneaker.js';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js'
import RetrieveAllSneakers from './pages/RetrieveAllSneakers.js'
import RetrieveAllActiveSneakers from './pages/RetrieveAllActiveSneakers.js'
import SneakerView from './pages/SneakerView.js'
import UpdateSneakerInformation from './pages/UpdateSneakerInformation.js'

import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';

export default function App(){
	
	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

  const unsetUser = () => {

    localStorage.clear()

  }

 useEffect(() => {
   if(localStorage.getItem('token')){

     fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
       method: 'GET',
       headers: {
         Authorization: `Bearer ${localStorage.getItem('token')}`
       }
     })
     .then(result => result.json())
     .then(data => {
       setUser({
         id: data._id,
         isAdmin: data.isAdmin
       });
     })
   }

 },[])

	return(
		<UserProvider value = {{user, setUser, unsetUser}}>
				<BrowserRouter>
					<Routes>
						<Route path = '/register' element = {
							user.id !==null
							?
							<PageNotFound/>
							:
							<>
							<AppNavBar />
							<Register/>
							</>
						} />

						<Route path = '/login' element = {
							user.id !==null
							?
							<PageNotFound/>
							:
							<>
							<AppNavBar />
							<Login/>
							</>
						} />

						<Route path = '/allSneakers' element = {
							user.id !==null && user.isAdmin
							?
							<>
								<AppNavBar />
								<RetrieveAllSneakers/>
							</>
							
							:
							<PageNotFound/>
						}/>

						<Route path = '/addSneaker' element = {
							user.id !==null && user.isAdmin
							?
							<>
								<AppNavBar />
								<AddSneaker/>
							</>

							:
							<PageNotFound/>
						} />

						<Route path = '/allSneakers/:sneakerId' element = {
							<>
								<AppNavBar />
								<UpdateSneakerInformation/>
							</>
						}/>

						<Route path = '/logout' element = {<Logout/>} />

						<Route path = '/' element = {
							<>
								<AppNavBar />
								<Home/>
							</>} />

						<Route path = '/activeSneakers' element ={
							<>
							<AppNavBar />
							<RetrieveAllActiveSneakers/>
							</>
						}/>

						<Route path = '/activeSneakers/:sneakerId' element = {
							<>
							<AppNavBar />
							<SneakerView/>
							</>
						}/>

						<Route path = '*' element = {<PageNotFound/>} />

					</Routes>
				</BrowserRouter>
		</UserProvider>
		)
}


