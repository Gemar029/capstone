const mongoose = require("mongoose");

const sneakerSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Sneakers name is required!"]
	},

	description: {
		type:String,
		required: [true, "Sneakers description is required!"]
	},

	price: {
		type: Number,
		required: [true, "Sneakers price is required!"]
	},

	size: {
		type: Number,
		required: [true, "Size of sneaker is required"]
	},

	stocks: {
		type: Number,
		required: [true, "Stock number is required!"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
})

const Sneakers = mongoose.model("Sneakers", sneakerSchema);
module.exports = Sneakers;