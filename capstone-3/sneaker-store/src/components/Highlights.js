import {Container, Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){

	return(
		<Container className = "mt-5">
			<Row>
				<Col className = "col-12 col-md-4 mt-3">
					<Card className = "cardHighlight">
					      <Card.Body>
					        <Card.Title>Shop at the comfort of your home</Card.Title>
					        <br/>
					        <Card.Text>
					        Shop at the comfort of your home just register/login using your account click SHOP NOW button above and choose the product you like and we will delivery the product to you safe and guaranteed Authentic and Original Money back guaranteed! if proven fake
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>

				<Col className = "col-12 col-md-4 mt-3">
					<Card className = "cardHighlight">
					      <Card.Body>
					        <Card.Title>What Payment Option Can You Use?</Card.Title>
					        <br/>
					        <Card.Text>
					        <ul>
					        	<li>Visa, MasterCard, Debit Card, Credit Card</li>
					        	<li>Google Pay</li>
					        	<li>GrabPay</li>
					        	<li>Gcash</li>
					        	<li>Paypal</li>
					        	<li>Paymaya</li>
					        	<li>Cash on Delivery</li>
					        </ul>
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>

				<Col className = "col-12 col-md-4 mt-3">
					<Card className = "cardHighlight">
					      <Card.Body>
					        <Card.Title>Fast Delivery!</Card.Title>
					        <br/>
					        <Card.Text>
					        	<Card.Subtitle>Standard Shipping</Card.Subtitle>
					        	<ul>
					        		<li>Free for order of ₱5,000 or more</li>
					        		<li>₱250 for orders less than ₱5,000</li>
					        		<li>Arrives in 2-5 business days</li>
					        	</ul>

					        	<Card.Subtitle>Express Shipping</Card.Subtitle>
					        	<ul>
					        		<li>₱500</li>
					        		<li>Arrives in 2-3 business days</li>
					        	</ul>

					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>
		)
}