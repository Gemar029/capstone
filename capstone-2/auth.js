const jwt = require("jsonwebtoken");

const secret ="SecretWalangClue";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		firstName: user.firstName,
		isAdmin: user.isAdmin,
		email: user.email,
		userName: user.userName
	}
	return jwt.sign(data, secret,{})
}

module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;

	if (token !== undefined){
		token = token.slice(7);

		return jwt.verify(token, secret, (error,data) =>{
			if (error){
				return res.send("Authentication Failed!")
			}else{
				next();
			}
		})
	}else{
		return res.send("No Token provided")
	}
}

module.exports.decode = (token) => {
	token = token.slice(7, token.length);

	return jwt.decode(token, {complete: true})
	.payload;

}