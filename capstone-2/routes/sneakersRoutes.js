const express = require("express");
const sneakersControllers = require("../controllers/sneakersControllers.js")
const auth = require("../auth.js")

const router = express.Router();
	// Routes
// ---------------------[Without Parameters]------------------------
	
		// routes for adding product
		router.post("/addSneaker", auth.verify, sneakersControllers.addSneaker)

		// routes for getting allSneakers
		router.get("/allSneakers", auth.verify, sneakersControllers.getAllSneakers)

		// routes for retrieving all active Sneakers
		router.get("/allActiveSneakers", sneakersControllers.getAllActiveSneakers)


// ------------------------[With Parameters]------------------------
		
		// routes for retrieving specific sneaker using Id
		router.get("/:sneakerId", sneakersControllers.getSingleSneaker)

		// routes for putting sneakers to archive and unarchive
		router.patch("/:sneakerId/archive", auth.verify, sneakersControllers.archiveSneaker)

		//routes for updating sneaker information
		router.patch("/:sneakerId/updateSneaker", auth.verify, sneakersControllers.updateSneaker)




module.exports = router