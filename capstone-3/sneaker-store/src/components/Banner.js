// Manual import of module form package.
// import Button from 'react-bootstrap/Button';
// import Container from 'react-bootstrap/Container';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

// Object destructuring
import {Button, Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){

	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					<h1>Sneaker Store</h1>
					<p>All products are guaranteed 100% Authentic/Original</p>
					<Button as = {Link} to = '/activeSneakers'>SHOP NOW!</Button>
				</Col>
			</Row>
		</Container>

		)
}