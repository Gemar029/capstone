// [Model Schema]
const Users = require("../models/Users.js");

const Orders = require("../models/Orders.js");

const Sneakers = require("../models/Sneakers.js")

// [Auth JWT]
const auth = require("../auth.js")

// Controller Creating Order
// Reference https://stackoverflow.com/questions/48601249/how-to-return-mongoose-query-results-without-callback

module.exports.createOrder = async (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		return res.send(false)
	}else{

				const sneakerId = req.body.sneakerId;
				const quantity = req.body.quantity;

				const sneaker = await Sneakers.findById(sneakerId)
				.then(result => (result))
				.catch(error => res.send(false))

				const totalAmount = quantity*sneaker.price

				let newOrder = new Orders({
					userId: userData.id,
					name: userData.firstName,
					sneakers: [{
						sneakerId: sneaker.id,
						sneakerName: sneaker.name,
						quantity: quantity
					}],
					totalAmount: totalAmount
				})



			newOrder.save()
			.then(result => res.send(true))
			.catch(error => res.send(false))
	}

}

// Controller for getting All Orders

module.exports.getAllOrders = (req, res) => {
		const userData = auth.decode(req.headers.authorization);
		
		if(!userData.isAdmin){

			return res.send(`You are not an Admin you do not have access to this route`)
		}else{

			Orders.find({})
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
}

module.exports.retrieveUsersOrders = (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	const userId = {userId : userData.id}

	Orders.find(userId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}