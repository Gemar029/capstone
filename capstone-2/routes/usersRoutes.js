const express = require("express");
const usersControllers = require("../controllers/usersControllers.js")
const auth = require("../auth.js")

const router = express.Router();

// Routes
//----------------------[Without Parameters]------------------------
	
	// routes for registration
	router.post("/register", usersControllers.registerUser)

	// routes for login
	router.post("/login", usersControllers.loginUser)


// ----------------------[With Parameters]--------------------------

	// routes for retrieving users details
	router.get("/userDetails", auth.verify, usersControllers.getUserDetails)

	// [Stretch Goal]
	// routes for changing user status to Admin or Non-Admin
	router.patch("/:userId/toAdmin", auth.verify, usersControllers.toAdmin)




module.exports = router
