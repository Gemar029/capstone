import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import myImage from './images/ZoroLost.jpg';
import React from 'react'


export default function PageNotFound(){
	return(
		<Container className = "mt-5">
			<Row>
				<Col className = "text-center">
					<img src = {myImage} alt = "Zoro Lost Again Meme"/>
					<h1>Page Not Found</h1>
					<p>
					Go back to the <Link to = '/' className='no-decoration'>homepage.</Link>
					</p>
				</Col>
			</Row>
		</Container>

		)
}