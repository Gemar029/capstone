const express = require("express");

// [Orders Controller]
const ordersControllers = require("../controllers/ordersControllers.js")

// [Auth JWT]
const auth = require("../auth.js")
const router = express.Router()

// [Routes]
	
	// router for making order
	router.post("/makeOrder", auth.verify, ordersControllers.createOrder)

	// rooter for getting all orders
	router.get("/allOrder", auth.verify, ordersControllers.getAllOrders)

	// router for authenticated users orders
	router.get("/authUser", auth.verify, ordersControllers.retrieveUsersOrders)

module.exports = router