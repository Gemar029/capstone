import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import AllSneakersCard from '../components/AllSneakersCard.js'
import {useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function RetrieveAllSneakers(){
	const[sneakers, setSneakers] = useState([]);
	// const[isActive, setIsActive] = useState(true)

		const archive = (_id, status) => {
			// console.log(_id)
			console.log(status)
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/${_id}/archive`, {
			method: 'PATCH',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				isActive: status
			})

		})
		.then(response => response.json())
		.then(data =>{
			if(data){
				if(status){

					Swal2.fire({
					    title: 'Successfully uArchived!',
					    icon: 'success',
					    text: 'You have successfully unArchived this course!'
					});
				}else{

					Swal2.fire({
					    title: 'Successfully Archived!',
					    icon: 'success',
					    text: 'You have successfully Archived this course!'
					});
				}
			}else{
				Swal2.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again or make sure you are login as Admin'
                    })
			}
		})
	}
	
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/allSneakers`,{
			method: 'GET',
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		})
		.then(result => result.json())
		.then(data => {
			setSneakers(data)
		})
	})

	return(
		<>
		<Container fluid='md' id = '#card-height' className = 'mb-3'>
			<Row className = 'd-flex align-items-center justify-content-center ' >
				<h1 className = 'text-center mt-3 mb-3'>All Sneakers</h1>
				{sneakers.map(sneaker => {
					return(
						<AllSneakersCard
									onArchive = {() => {
										// console.log(false)
						        		// setIsActive(false)
						        		archive(sneaker._id, false)
						        	}}
						        	onActivate = {() => {
						        		// console.log(true)
						        		// setIsActive(true)
						        		archive(sneaker._id, true)
						        	}}
						        	key = {sneaker._id} sneakerProp = {sneaker}/>
					)
				})}
			</Row>
		</Container>
		</>
		)
}